package com.wgs.base.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Builder 
@AllArgsConstructor
@Getter 
@Setter
public class BaseResponseMessage<T> {

    private String statusCode;
    private String message;
    private List<BaseErrorMessage> errorMessages;
    private T data;
    
    public BaseResponseMessage() {
        this.errorMessages = new ArrayList<>();
    }

    public void setStatusCode(String statusCode) {
    	this.statusCode = statusCode;
    }
    
    public void setStatusCode(ResponseCodeEnum responseCodeEnum) {
        this.statusCode = responseCodeEnum.getCode();
        this.message = responseCodeEnum.getDescription();
    }
    
}
