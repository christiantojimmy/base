package com.wgs.base.service;

import java.util.List;

import com.wgs.base.model.BaseModel;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.response.ResponseCodeEnum;

public interface BaseService <B extends BaseModel, ID, RequestDto, ResponseDto> {

	public List<ResponseDto> viewAll();
	
	default public boolean beforeView(BaseResponseMessage baseResponseMessage, RequestDto request){
        return true;
    }
    default public boolean view(BaseResponseMessage baseResponseMessage, RequestDto request) {
        return true;
    }
	
	default public boolean beforeSave(BaseResponseMessage baseResponseMessage, RequestDto request){
		return true;
	}
	default public boolean save(BaseResponseMessage baseResponseMessage, RequestDto request){
		return true;
	}
	
	default public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, RequestDto dto){
        return true;
    }
    default public boolean update(BaseResponseMessage baseResponseMessage, RequestDto dto){
        return true;
    }
    
    default public boolean beforeDelete(BaseResponseMessage baseResponseMessage, RequestDto request){
        return true;
    }
    default public boolean delete(BaseResponseMessage baseResponseMessage, RequestDto request){
        return true;
    }
    
    default public boolean checkExist(BaseResponseMessage baseResponseMessage, RequestDto request){
        return true;
    }
    
    default public boolean doView(BaseResponseMessage baseResponseMessage, RequestDto request) {
    	if (beforeView(baseResponseMessage, request)) {
    		if (view(baseResponseMessage, request)) {
    			return true;
    		} else {
    			baseResponseMessage.setStatusCode(ResponseCodeEnum.VIEW_FAILED);
    			return false;
    		}
    	} else {
    		baseResponseMessage.setStatusCode(ResponseCodeEnum.BEFORE_VIEW_FAILED);
    		return false;
    	}
    }
    
    default public boolean doSave(BaseResponseMessage baseResponseMessage, RequestDto request) {
		if (beforeSave(baseResponseMessage, request)) {
			if (save(baseResponseMessage, request)) {
				return true;
			} else {
				baseResponseMessage.setStatusCode(ResponseCodeEnum.SAVE_FAILED);
				return false;
			}
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.BEFORE_SAVE_FAILED);
			return false;
		}
	}
    
    default public boolean doUpdate(BaseResponseMessage baseResponseMessage, RequestDto request) {
    	if (beforeUpdate(baseResponseMessage, request)) {
    		if (update(baseResponseMessage, request)) {
    			return true;
    		} else {
    			baseResponseMessage.setStatusCode(ResponseCodeEnum.UPDATE_FAILED);
    			return false;
    		}
    	} else {
    		baseResponseMessage.setStatusCode(ResponseCodeEnum.BEFORE_UPDATE_FAILED);
    		return false;
    	}
    }
    
    default public boolean doDelete(BaseResponseMessage baseResponseMessage, RequestDto request) {
    	if (beforeDelete(baseResponseMessage, request)) {
    		if (delete(baseResponseMessage, request)) {
    			return true;
    		} else {
    			baseResponseMessage.setStatusCode(ResponseCodeEnum.DELETE_FAILED);
    			return false;
    		}
    	} else {
    		baseResponseMessage.setStatusCode(ResponseCodeEnum.BEFORE_DELETE_FAILED);
    		return false;
    	}
    }
	
	
}
